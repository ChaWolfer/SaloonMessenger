<!-- HEADER -->
@include ('includes/_header')

<h1>
    Connectez vous !
</h1>

<div class="row">
        Pas de comptes ? Venez vous inscrire !
        <a href="{{ route('url_to_inscription') }}">
           <button type="button" class="btn btn-success"> S'inscrire !</button>
        </a>
</div>



<form class="row" action="{{route('url_to_login')}}" method="post">
    {{csrf_field()}}
    <!-- Clé permettant l'envoie du form -->
    <div class="col-sm-6 col-xs-12 margin_on">
        <span>Mail</span>
        <input type="email" name="mail" value="">
    </div>
    <div class="col-sm-6 col-xs-12 margin_on">
        <span>Mot de passe</span>
        <input type="password" name="password" value="">
    </div>
    <div class="col-sm-3 col-xs-12 margin_on">
        <input type="submit" name="Envoyer" value="Envoyer">
    </div>
    <!-- Vérifie si les champs sont tous remplis -->
    @if (session('erreur_login'))
        <div class="erreur" style="color: red;">
            {{ session('erreur_login') }}
        </div>
    @endif
    <!-- Affiche les messages d'erreurs -->
    @if (session('requete'))
        <div class="erreur" style="color: red;">
            {{ session('requete') }}
        </div>
    @endif
</form>

<!-- FOOTER -->
@include ('includes/_footer')
